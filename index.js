const express = require('express');
const app = express();
const path = require('path');
const contactReq = [];
const PORT = 8000;


// Adding some middleware to be able to deconstruct the data sent from the contact-form
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Set static folder
app.use(express.static(path.join(__dirname, 'public')));

//app.get('/', (res, req) => {
//    return res.sendFile(path.join(__dirname, 'public', 'index.html'));
//});


// Contact request from contact.html II second attempt
app.post('/contact', (req, res) => {
    const newContactReq = {
        id: contactReq.length + 1,
        name: req.body.contactName,
        mail: req.body.contactEmail,
        message: req.body.contactMessage,
        status: 'active'
    }

    if(!newContactReq.name || !newContactReq.mail || !newContactReq.message ) {
        return res.status(400).json({ msg: 'Please fill in all fields in the form' });
    }

    contactReq.push(newContactReq);
    res.json(contactReq);
    console.log(contactReq);
});



// Destructure the port from the environment variale if not found make it 3000
// const { PORT = 3000 } = process.env;

// Open up a server with a port to listen to
app.listen(PORT, () => console.log(`Server has started on port ${PORT}...`)); 