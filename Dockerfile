# 1 Specify the Image we want to work from
FROM node:latest

# 2 Specify workin g directory in the new image
WORKDIR /usr/src/app

# 3 Copy resources - package.json & package-lock.json to the root
COPY package*.json ./

# 4 Install package dependencies
RUN npm install

# 5 Bundeour sourcecode
COPY . .

# ENV PORT 8000

# 6 Expose the port - blocked by default.
EXPOSE 8000

# 7 Command that executes when image launches
# Changed from CMD ["node","index.js"] previously
CMD ["npm", "start"]